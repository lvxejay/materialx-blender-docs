.. MaterialX documentation master file, created by
   sphinx-quickstart on Fri Nov 17 18:32:01 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MaterialXBlender Reference & Documentation
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   user_guide/index
   dev_guide/index
   release_notes/index

External Links
**************

**MaterialX Home**
   http://www.materialx.org


**MaterialX Specification**
   http://www.materialx.org/assets/MaterialX.v1.35.Final.pdf


**Blender 3D**
   http://www.blender.org





