proteus\.properties package
===========================

Submodules
----------

proteus\.properties\.dynamic\_property module
---------------------------------------------

.. automodule:: proteus.properties.dynamic_property
    :members:
    :undoc-members:
    :show-inheritance:

proteus\.properties\.node\_parameters module
--------------------------------------------

.. automodule:: proteus.properties.node_parameters
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: proteus.properties
    :members:
    :undoc-members:
    :show-inheritance:
