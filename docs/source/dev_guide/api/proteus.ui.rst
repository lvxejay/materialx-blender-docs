proteus\.ui package
===================

Submodules
----------

proteus\.ui\.material\_properties module
----------------------------------------

.. automodule:: proteus.ui.material_properties
    :members:
    :undoc-members:
    :show-inheritance:

proteus\.ui\.node\_menu module
------------------------------

.. automodule:: proteus.ui.node_menu
    :members:
    :undoc-members:
    :show-inheritance:

proteus\.ui\.node\_panel module
-------------------------------

.. automodule:: proteus.ui.node_panel
    :members:
    :undoc-members:
    :show-inheritance:

proteus\.ui\.node\_tree\_panel module
-------------------------------------

.. automodule:: proteus.ui.node_tree_panel
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: proteus.ui
    :members:
    :undoc-members:
    :show-inheritance:
