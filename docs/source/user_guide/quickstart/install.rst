Installation Guide
==================

1) Download from here: https://gitlab.com/lvxejay/materialx-blender
2) Open User Preferences in Blender and install the addon

.. image:: ./../../images/userprefs.png

3) Press Install Requirements
4) Press Install MaterialX
5) Restart Blender